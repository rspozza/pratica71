
import java.util.ArrayList;
import java.util.Collections;
import java.util.Scanner;
import utfpr.ct.dainf.if62c.pratica.Jogador;

/**
 * UTFPR - Universidade Tecnológica Federal do Paraná DAINF - Departamento
 * Acadêmico de Informática
 *
 * Template de projeto de programa Java usando Maven.
 *
 * @author Wilson Horstmeyer Bogado <wilson@utfpr.edu.br>
 */
public class Pratica71 {

    public static int getIntFromScanner(Scanner sc, String message, String errorMessage) {
        System.out.println(message);
        while (!sc.hasNextInt()) {
            System.err.println(errorMessage);
            sc.next();
        }
        return sc.nextInt();
    }

    public static void main(String[] args) {
        int numero = 0;
        String nome;
        boolean invalido = true;
        ArrayList<Jogador> time = new ArrayList<>();
        int numJogadores;
        Scanner scanner = new Scanner(System.in);
        System.out.print("Numero de jogadores a serem lidos:");
        numJogadores = scanner.nextInt();
        for (int i = 0; i < numJogadores; i++) {            
            numero=getIntFromScanner(scanner, "Digite o numero do jogador", "Apenas o numero");
            System.out.print("Digite o nome do Jogador:");
            nome = scanner.next();
            time.add(new Jogador(numero, nome));
        }

        Collections.sort(time,
                new CompararJogador());

        for (Jogador j : time) {
            System.out.println(j);
        }
        // armazenar cada jogador numa lista
        // verificar se o usuario digitou um numero valido
        // ordenar a ista por ordem crescente de numero e exiba-la

        // executar um laco que termine com 0. Usuario digita inclui mais jogadores a lista
        // caso um numero ja exista, seu nome deve ser atualizado
        // caso nao exista, deve ser inserido na posicao correta a que a lista continue ordenada
        // apos cada leitura exiba a lista novamente verificando que esta permanece ordenada
    }
}
