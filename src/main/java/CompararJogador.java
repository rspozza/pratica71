
import java.util.Comparator;
import utfpr.ct.dainf.if62c.pratica.Jogador;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author pozza
 */
public class CompararJogador implements Comparator<Jogador>{

    @Override
    public int compare(Jogador o1, Jogador o2) {
        if(o1.getNumero()<o2.getNumero())
            return -1;
        else return 1;
    }
    
}
