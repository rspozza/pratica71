/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package utfpr.ct.dainf.if62c.pratica;

import java.util.Comparator;

/**
 *
 * @author pozza
 */
public class JogadorComparator implements Comparator<Jogador>{
    
    private boolean numero; // se verdadeiro, indica ordenação por número
    private boolean num_asc; // se verdadeiro, indica que desejamos ordenar por número ascendente
    private boolean nome_asc; // se verdadeiro, indica que desejamos ordenar por nome ascendente
    
    public JogadorComparator(){
        this.num_asc=this.nome_asc=true;
    }
    
    public JogadorComparator(boolean numero, boolean num_asc, boolean nome_asc){
        this.numero=numero;
        this.num_asc=num_asc;
        this.nome_asc=nome_asc;
    }

    @Override
    public int compare(Jogador o1, Jogador o2) {
        return o1.compareTo(o2);
    }
    
}
